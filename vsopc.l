%option noyywrap 
%option debug
%option c++
%option yylineno
%{
      #include <iostream>
      #include <algorithm>
      #include <stdio.h>
      #include <stdlib.h>
      #include <map>
      #include <unordered_map>
      #include <string>
      #include <fstream> 
     // std::ifstream
      #include <sstream>
      #include <iterator>
      #include <iomanip>
      #include <regex>

//#define YY_USER_ACTION yycolumn += yyleng;
	using namespace std;
	
int start_line, start_column;
int yycolumn = 1;

/* Forward declarations */
void report(const char* ttype,const char* yy, int line, int column);
void reportKeys(const char* t,  int l, int c);
void find_and_replace(string& source, string const& find, string const& replace);
//string RemoveChar(string str, const char* c);
/* This is executed before every action. */
#define YY_USER_ACTION                                                   \
      start_line = prev_yylineno; start_column = yycolumn;                   \
     if (yylineno == prev_yylineno) yycolumn += yyleng;                     \
     else {                                                                 \
      for (yycolumn = 1; yytext[yyleng - yycolumn] != '\n'; ++yycolumn) {} \
      prev_yylineno = yylineno;                                            \
            }

FILE* out_txt = fopen("test2.txt", "w");
int mylineno = 1;
	map<string, string> keywords = 
{
	    { "and", "and" },
	    { "extends", "extends" },
	    { "isnull" , "isnull" },
	    { "then" ,  "then" },
	    { "bool" ,  "bool" },
	    { "false" ,  "false" },
	    { "let" ,  "let" },
	    { "true" ,  "true" },
	    { "class" ,  "class" },
	    { "if" ,  "if" },
	    { "new" ,  "new" },
	    { "unit" ,  "unit" },
	    { "do" ,  "do" },
	    { "in" ,  "in" },
	    { "not" ,  "not" },
	    { "while" ,  "while" },
	    { "else" ,  "else" },
	    { "int32" ,  "int32" },
	    { "string" ,  "string" }
	};
	

	// Variables :
    	int lineNb = 1;
    	//int columnNb = 1;
	//int yycolumn =1;
	// Functions :
	/*int getColNb(int lengthCurrentWord)
	{
	yycolumn -= lengthCurrentWord;
        cout <<"\n"<< lineNb << " , " << yycolumn  ;
        //yycolumn += lengthCurrentWord;
	}*/
%}

string 			\"[^\n"]+\"

ws     			[ \t]+
lowercase-letter 	[a-z]
uppercase-letter 	[A-Z]
letter 			{lowercase-letter}|{uppercase-letter}

bin-digit		[0-1]
digit     		{bin-digit}|[2-9]
hex-digit		{digit}|[a-fA-F]

type-identifier		{uppercase-letter}({letter}|{digit}|_)*
object-identifier       {lowercase-letter}({letter}|{digit}|_)*

regular-char      [{lowercase-letter}{uppercase-letter}{digit}_]
escaped-char      (\\[bfnrtv0'"\\])
/*
//regular-char [a-zA-Z0-9_]
//escaped-char (\\[bfnrtv0'"\\])|\\
//need to be put in "" [a-zA-Z0-9_]|(\\[bfnrtv0'"\\])|\\
*/

string-literal    \"(([^\"]|\\\")*[^\\])?\"
integer-literal2		{digit}|0x{hex-digit}|0b{bin-digit}
integer-literal         0[xXbB][0-9a-fA-F]+|(-\d|[0-9]+)

/*
name    		({letter}|{digit}|\$)({letter}|{digit}|[_.\-/$])*
num1    		[-+]?{digit}+\.?([eE][-+]?{digit}+)?
num2    		[-+]?{digit}*\.{digit}+([eE][-+]?{digit}+)?
number  		{num1}|{num2}
*/

/* Operators */
lbrace     		[{]
rbrace          [}]
assign            <-
colon             :
semicolon       ;
comma       ","
plus        "+"
minus       "-"
times       "*"
div         "/"
pow         "^"
dot         "."
equal       "="
lower       "<"
lower-equal "<="

comment_single  "\\"
%x COMMENT
%x comsgle_lexer

%%
      /* Any indented text before the first rule goes at the top of the lexer.  */
   int start_line, start_column;
   int prev_yylineno = yylineno; 

{ws}    /* skip blanks and tabs */

"//".*      {}

{type-identifier}		{	//getColNb(YYLeng());
					//printf(",type-identifier,%s", YYText());
                              report("type-identifier",YYText(), start_line,        start_column);
				}
{object-identifier}     {
                             auto k = keywords.find(string(YYText()));			
			      if (k != keywords.end())
                        {reportKeys(YYText(), start_line, start_column);}
                else
                    {report("object-identifier",YYText(), start_line,             start_column);}
                        }

{lbrace}    {reportKeys("lbrace", start_line,     start_column);}
{rbrace}    {reportKeys("rbrace", start_line,     start_column);}
{assign}    {reportKeys("assign", start_line,     start_column);}
{colon}    {reportKeys("colon", start_line,     start_column);}
{pow}   {reportKeys("pow", start_line,     start_column);}
{div}   {reportKeys("div", start_line,     start_column);}
{times} {reportKeys("times", start_line,     start_column);}
{minus} {reportKeys("minus", start_line,     start_column);}
{plus}  {reportKeys("plus", start_line,     start_column);}
{semicolon} {reportKeys("semicolon", start_line,     start_column);}
{dot}   {reportKeys("dot", start_line,     start_column);}
{equal} {reportKeys("equal", start_line,     start_column);}
{lower} {reportKeys("lower", start_line,     start_column);}
{lower-equal}   {reportKeys("lower-equal", start_line,     start_column);}


{string-literal}        {

string yy_scan = yytext;
find_and_replace(yy_scan,"\n","");
                        find_and_replace(yy_scan,"\\n","\\x0a");
                        find_and_replace(yy_scan,"\\r","\\x0d");
                        find_and_replace(yy_scan,"\\t","\\x09");
                        find_and_replace(yy_scan,"\\f","\\x0c");
                        find_and_replace(yy_scan,"\\b","\\x08");
                        find_and_replace(yy_scan,"\t","");
                        find_and_replace(yy_scan,"\\ ","");
        regex e ("(\\\\)([^x0])"); //match_results< std::string::const_iterator > mr;
//regex_search(yy_scan,mr,e);
//cout << mr[1] << '\n';
//cout << regex_replace (yy_scan, e, "$2") << "\n";
yy_scan = regex_replace (yy_scan, e, "$2");


//yy_push_state(comsgle_lexer);
                        /*if (yy_scan.find("\\"))
{if(yy_scan.find("\\ "))
    {find_and_replace(yy_scan,"\\ ",""); }
else {find_and_replace(yy_scan,"\\","");}

}*/
                        
                        const char* yy = yy_scan.c_str();
                        report("string-literal",yy, start_line,     start_column);
                        //printf(string(yy_scan_string));
                        //report("string-literal",YYText(), start_line,     start_column);
            }
{integer-literal}       {
                  string num = YYText();                  
                  if (num.find("0x") != string::npos)
                  { int xx =stoi(num,nullptr,0); string s = to_string(xx);
report("integer-literal",s.c_str(), start_line,     start_column);
          }
else if (num.find("0b") != string::npos) 
{num.erase(0,2); int x2 = stoi(num,nullptr,2); string s2 = to_string(x2);
report("integer-literal",s2.c_str(), start_line,     start_column);}

else {
num.erase(0,num.find_first_not_of('0')); if(num.empty()){num = "0";}
report("integer-literal",num.c_str(), start_line,     start_column);}
                 }

    /*{comment_single} {BEGIN(comsgle_lexer);}
    //<comsgle_lexer>(\n|\f)  {BEGIN(INITIAL);} */

<comsgle_lexer>{
"\\"    {yy_pop_state();}
}
                                           
"(*"        {yy_push_state(COMMENT);} 

<COMMENT>{   
    "*)"    {yy_pop_state();}
    "(*"    {yy_push_state(COMMENT);}
    "*"+    ;
    "//"+   ;
    "\n"    ;
    [ \t]+  ;
    [a-zA-Z0-9]   ;
    ")"     ;
    [+|-|,|\"]* ;
    [-|..] ;
    
}                 
"/*"    {        
        
        int c;

        while((c = yyinput()) != 0)
            {
            if(c == '\n')
                ++mylineno;

            else if(c == '*')
                {
                if((c = yyinput()) == '/')
                    break;
                else
                    unput(c);
                }
            }
        }

	/*/if|class|begin|end|procedure|function        {
            //printf( "A keyword: %s\n", yytext );
            //}*/


\n        { lineNb++; yycolumn = 1;  }

    /*{name}    	{ 
			auto k = keywords.find(string(YYText()));			
			if (k != keywords.end())
			//if(keywords.find(YYText()))
			{
			//getColNb(YYLeng());
			//printf(",%s:w_lenght %d", YYText(), YYLeng());
                  reportKeys(YYText(), start_line, start_column);
			}
			//else
			//{printf("name,%s", YYText());	}
			//cout << "name " << YYText() << '\n';
		}*/


%%
void find_and_replace(string& source, string const& find, string const& replace)
{
    for(string::size_type i = 0; (i = source.find(find, i)) != string::npos;)
    {
        source.replace(i, find.length(), replace);
        i += replace.length();
    }
}
void report(const char* t, const char* yy, int l, int c) {
  printf("%d,%d,%s,%s\n",l, c, t, yy  );
  fprintf(out_txt,"%d,%d,%s,%s\n",l, c, t, yy  );
//fprintf(stdout,"%d,%d,%s,%s  \n",l, c, t, yy  );
}//not only keys and operators also)))
void reportKeys(const char* t,  int l, int c) {
  printf("%d,%d,%s\n",l, c, t  );
  fprintf(out_txt,"%d,%d,%s\n",l, c, t  );
    //fprintf(stdout,"%d,%d,%s  \n",l, c, t  );
}
/*string RemoveChar(string str,const char* c) 
{
   string result;
   for (size_t i = 0; i < str.size(); i++) 
   {
          char currentChar = str[i];
          if (currentChar != c)
              result += currentChar;
   }
       return result;
}*/
int main(int argc, char*argv[]) {
    std::ifstream ifs;
    //ifs.open ("vsop.txt", std::ifstream::in);    
    //ifs.open (argv[1], std::ifstream::in);
    //FlexLexer* lexer = new yyFlexLexer(ifs, cout);
    //lexer->yylex();
      //while(lexer->yylex() != 0)
	//;
      //ifstream qFile(argv[1]); string qLine;
      //while(!qFile.eof()) {
      //getline(qFile, qLine);
      
      //cout << qLine << endl;
      //}
ifs.open (argv[2], std::ifstream::in);
FlexLexer* lexer = new yyFlexLexer(ifs, cout);
    while(lexer->yylex() != 0)      ;

    return 0;
}

