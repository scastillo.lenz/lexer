.PHONY: debug, install-tools

CFLAGS=-lfl 

all:
	${MAKE} lex
	#gcc -o vsopc lex.yy.c ${CFLAGS}
	g++ --std=c++11 -o vsopc lex.yy.cc  ${CFLAGS}


lex:
	#flex vsopc.l
	flex vsopc.l

install-tools:
	sudo apt-get -y install flex


tarxz:
	rm vsopcompiler.tar.xz
	#cp vsopc.l vsopcompiler/
	cp vsopc.l vsopcompiler/


debug:
	./vsopc -flex test.txt

valid:
	./vsopc -flex test2.txt
